#include <cstdlib>
#include <unordered_map>
#include <algorithm>
#include "lua/lua.hpp"
#include "lua/lstate.h"
#include "types.hpp"
#include "string.hpp"
#include "robloxlibrary.hpp"

/*
- bool		HookMethod(dword address, Value callback, Type... types)
- bool		UnhookMethod(dword address)
- bool		EnableHook(dword address)
- bool		DisableHook(dword address)
- Dword		Call(dword func, dword self, Type[] argTypes, Value... args)
*/

std::unordered_map<ptr, HookData *>hooks;

static int HookMethod(lua_State *L) {
	ptr hookPtr = GetPointer(L, 1);
	dword typeCount = lua_gettop(L) - 2;

	if (hooks.find(hookPtr) != hooks.end()) {	// Hook exists
		lua_pushboolean(L, 0);
		return 1;
	}

	lua_pushvalue(L, 2);
	auto hookData = new HookData(hookPtr, *(ptr *)hookPtr, typeCount, luaL_ref(L, LUA_REGISTRYINDEX));
	for (dword idx = 0; idx < typeCount; idx++) {
		hookData->argTypes[idx] = GetInteger(L, idx + 3);
	}

	hooks[hookPtr] = hookData;
	lua_pushboolean(L, 1);
	return 1;
}

static int UnhookMethod(lua_State *L) {
	ptr hookPtr = GetPointer(L, 1);
	auto hookIter = hooks.find(hookPtr);
	if (hookIter == hooks.end()) {	// Hook doesn't exist
		lua_pushboolean(L, 0);
		return 1;
	}

	*(ptr *)hookIter->second->hookPtr = hookIter->second->funcPtr;
	delete hookIter->second;
	hooks.erase(hookPtr);
	lua_pushboolean(L, 1);
	return 1;
}

static int EnableHook(lua_State *L) {
	ptr hookPtr = GetPointer(L, 1);
	auto hookIter = hooks.find(hookPtr);
	if (hookIter == hooks.end()) {	// Hook doesn't exist
		lua_pushboolean(L, 0);
		return 1;
	}

	*(ptr *)hookPtr = (ptr)hookIter->second->hookFunc;
	lua_pushboolean(L, 1);
	return 1;
}

static int DisableHook(lua_State *L) {
	ptr hookPtr = GetPointer(L, 1);
	auto hookIter = hooks.find(hookPtr);
	if (hookIter == hooks.end()) {	// Hook doesn't exist
		lua_pushboolean(L, 0);
		return 1;
	}

	*(ptr *)hookPtr = (ptr)hookIter->second->funcPtr;
	lua_pushboolean(L, 1);
	return 1;
}

template <typename T>
static void WriteStack(ptr *base, dword &stkOff, T value) {
	*(T *)(base + stkOff) = value;
	stkOff += std::max<int>(sizeof(T) / 4, 1);
}

static int Call(lua_State *L) {
	ptr func = GetPointer(L, 1);
	ptr self = GetPointer(L, 2);
	dword argc = lua_gettop(L) - 3;
	luaL_checktype(L, 3, LUA_TTABLE);

	// Arguments
	auto functionData = new FunctionData(func, argc);
	ptr *stk = argc > 0 ? new ptr[argc * (28 / 4)] : nullptr;
	dword stkOff = 0;
	auto *String2Cache = (String2 **)new ptr[argc * (28/4)];
	dword String2Count = 0;

	for (dword idx = 0; idx < argc; idx++) {
		dword argType;
		lua_rawgeti(L, 3, idx + 1);
		argType = GetInteger(L, -1);
		lua_pop(L, 1);
		switch (argType) {
			case Type::BYTE: WriteStack<byte>(stk, stkOff, GetInteger(L, idx + 4)); break;
			case Type::WORD: WriteStack<word>(stk, stkOff, GetInteger(L, idx + 4)); break;
			case Type::DWORD: WriteStack<dword>(stk, stkOff, GetInteger(L, idx + 4)); break;
			case Type::QWORD: WriteStack<qword>(stk, stkOff, GetInteger(L, idx + 4)); break;
			case Type::FLOAT: WriteStack<float>(stk, stkOff, (float)GetFloat(L, idx + 4)); break;
			case Type::DOUBLE: WriteStack<double>(stk, stkOff, GetFloat(L, idx + 4)); break;
			case Type::STRING: WriteStack<const char *>(stk, stkOff, luaL_checkstring(L, idx + 4)); break;
			case Type::STRING1: WriteStack<String1>(stk, stkOff, String1(luaL_checkstring(L, idx + 4))); break;
			case Type::STRING2: {
				String2 *str = new String2(luaL_checkstring(L, idx + 4));
				String2Cache[String2Count++] = str;
				WriteStack<String2>(stk, stkOff, *str);
				break;
			}
			default: DebugPrint("[Lua Error]: bad type"); return 0;
		}
	}

	//	Call
	dword stackSize = stkOff * 4;
	dword retnVal;

	__asm {
		CMP		dword ptr [argc], 0
		JZ		SetupSelf

		SUB		esp, [stackSize]	; stack_alloc(stackSize)
		MOV		esi, [stk]
		MOV		ecx, [stkOff]
		MOV		edi, esp
		CLD
		REP		MOVSD				; memcpy(stack, stk, stkOff)

		SetupSelf:
		CMP		dword ptr [self], 0
		JZ		CallFunc
		MOV		ecx, [self]

		CallFunc:
		CALL	func

		CMP		dword ptr [argc], 0
		JZ		Finish
		ADD		esp, [stackSize]

		Finish:
		MOV		[retnVal], eax
	}

	delete functionData;
	if (argc > 0) delete[] stk;
	if (String2Count > 0) {
		for (dword strIdx = 0; strIdx < String2Count; strIdx++) delete String2Cache[strIdx];
		delete[] String2Cache;
	}
	PushInteger(L, retnVal);
	return 1;
}

static luaL_Reg robloxlib[] = {
	{ "HookMethod", HookMethod },
	{ "UnhookMethod", UnhookMethod },
	{ "EnableHook", EnableHook },
	{ "DisableHook", DisableHook },
	{ "Call", Call },
	{ NULL, NULL }
};

void luaopen_roblox(lua_State *L) {
	luaL_newlib(L, robloxlib);
	lua_setglobal(L, "Roblox");
}

template <typename T>
static T ReadStack(ptr *base, dword &stkOff) {
	auto value = (T *)(base + stkOff);
	stkOff += std::max<int>(sizeof(T) / 4, 1);
	return *value;
}

static ptr HookHandler(ptr self) {
	HookData *hookData;
	ptr hook, *base;
	__asm {
		MOV		[hook], ebx
		MOV		[base], ebp
	}
	hookData = hooks.at(hook);

	// Call Lua
	auto top = L->top;
	luaL_checkstack(L, hookData->argc + 2, "too many arguments");
	lua_rawgeti(L, LUA_REGISTRYINDEX, hookData->luaFuncRef);
	PushInteger(L, self);
	dword stkOff = 2 + 8 + 1 + 1 + 1;		// (retn + self) + (8regs) + (local) + (retn) + (arg1)
	for (dword idx = 0; idx < hookData->argc; idx++) {
		switch (hookData->argTypes[idx]) {
			case Type::BYTE: PushInteger(L, ReadStack<byte>(base, stkOff)); break;
			case Type::WORD: PushInteger(L, ReadStack<word>(base, stkOff)); break;
			case Type::DWORD: PushInteger(L, ReadStack<dword>(base, stkOff)); break;
			case Type::QWORD: PushInteger(L, ReadStack<qword>(base, stkOff)); break;
			case Type::FLOAT: PushFloat(L, ReadStack<float>(base, stkOff)); break;
			case Type::DOUBLE: PushFloat(L, ReadStack<double>(base, stkOff)); break;
			case Type::STRING: lua_pushstring(L, ReadStack<const char *>(base, stkOff)); break;
			case Type::STRING1: lua_pushstring(L, ReadStack<String1>(base, stkOff).Data()); break;
			case Type::STRING2: lua_pushstring(L, ReadStack<String2>(base, stkOff).Data()); break;
			default: L->top = top; DebugPrint("[Lua Error]: bad type"); return hookData->funcPtr;
		}
	}
	if (lua_pcall(L, hookData->argc + 1, 0, 0) != LUA_OK) {
		DebugPrint((std::string("[Lua Error]: ") + lua_tostring(L, -1)).c_str());
		lua_pop(L, 1);
	}

	return hookData->funcPtr;
}

static void __declspec(naked) HookWrapper() {
	__asm {
		SUB		esp, 4
		PUSHAD						// EAX, ECX, EDX, EBX, ESP, EBP, ESI, EDI
		MOV		ebx, 0x00000000		// placeholder for hookPtr
		LEA		eax, [HookHandler]
		PUSH	ecx					// this (__thiscall)
		CALL	eax
		ADD		esp, 4
		MOV		[esp + 32], eax		// save return value
		POPAD
		POP		eax
		JMP		eax
	}
}
