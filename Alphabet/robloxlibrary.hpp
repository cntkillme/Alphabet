#pragma once
#include <Windows.h>
#include "lua/lua.hpp"
#include "types.hpp"
#include "string.hpp"

void HookWrapper();

class HookData {
public:
	const ptr hookPtr;
	const ptr funcPtr;
	const ptr self;
	const dword argc;
	const int luaFuncRef;
	byte *hookFunc;
	dword *argTypes;

	HookData(ptr hookPtr, ptr funcPtr, dword argc, int luaFuncRef, ptr self = 0) : hookPtr(hookPtr), funcPtr(funcPtr), argc(argc), luaFuncRef(luaFuncRef), self(self) {
		argTypes = (argc > 0) ? new dword[argc] : nullptr;
		hookFunc = new byte[64];
		std::memcpy(hookFunc, HookWrapper, 64);
		*(ptr *)(hookFunc + 5) = hookPtr;

		DWORD temp;
		VirtualProtect(hookFunc, 64, PAGE_EXECUTE_READWRITE, &temp);
	}

	~HookData() {
		luaL_unref(L, LUA_REGISTRYINDEX, luaFuncRef);
		delete[] hookFunc;
		if (argc > 0) delete[] argTypes;
	}
};

class FunctionData {
public:
	const ptr funcPtr;
	const dword argc;
	dword *argTypes;

	FunctionData(ptr funcPtr, dword argc) : funcPtr(funcPtr), argc(argc) {
		argTypes = (argc > 0) ? new dword[argc] : nullptr;
	}

	~FunctionData() {
		if (argc > 0) delete[] argTypes;
	}
};
