#include "lua/lua.hpp"
#include "types.hpp"
#include "string.hpp"

/*
- byte		ReadByte(dword address)
- word		ReadWord(dword address)
- dword		ReadDword(dword address)
- qword		ReadQword(dword address)
- float		ReadFloat(dword address)
- double	ReadDouble(dword address)
- byte...	ReadBytes(dword address, dword count)
- string	ReadString(dword address)
- string	ReadString1(dword address)
- string	ReadString2(dword address)

- void		WriteByte(dword address, byte value)
- void		WriteWord(dword address, word value)
- void		WriteDword(dword address, dword value)
- void		WriteQword(dword address, qword value)
- void		WriteFloat(dword address, float value)
- void		WriteDouble(dword address, double value)
- void		WriteBytes(dword address, byte...)
- void		WriteString(dword address, string value)
- dword		WriteString1(dword address, string value)
- dword		WriteString2(dword address, string value)

- void		FreeString1(dword address)
- void		FreeString2(dword address)
*/

template <typename T>
static T ReadMemory(ptr address) {
	return *(T *)address;
}

template <typename T>
static void WriteMemory(ptr address, T value) {
	*(T *)address = value;
}

static int ReadByte(lua_State *L) {
	PushInteger(L, ReadMemory<byte>(GetPointer(L, 1)));
	return 1;
}

static int ReadWord(lua_State *L) {
	PushInteger(L, ReadMemory<word>(GetPointer(L, 1)));
	return 1;
}

static int ReadDword(lua_State *L) {
	PushInteger(L, ReadMemory<dword>(GetPointer(L, 1)));
	return 1;
}

static int ReadQword(lua_State *L) {
	PushInteger(L, ReadMemory<qword>(GetPointer(L, 1)));
	return 1;
}

static int ReadFloat(lua_State *L) {
	PushFloat(L, ReadMemory<float>(GetPointer(L, 1)));
	return 1;
}

static int ReadDouble(lua_State *L) {
	PushFloat(L, ReadMemory<double>(GetPointer(L, 1)));
	return 1;
}

static int ReadBytes(lua_State *L) {
	ptr address = GetPointer(L, 1);
	dword count = GetInteger(L, 2);

	luaL_checkstack(L, count, "too many bytes");
	for (dword idx = 0; idx < count; idx++) {
		PushInteger(L, ReadMemory<byte>(address + idx));
	}
	return count;
}

static int ReadString(lua_State *L) {
	lua_pushstring(L, (const char *)GetPointer(L, 1));
	return 1;
}

static int ReadString1(lua_State *L) {
	lua_pushstring(L,  ((String1 *)GetPointer(L, 1))->Data());
	return 1;
}

static int ReadString2(lua_State *L) {
	lua_pushstring(L, ((String2 *)GetPointer(L, 1))->Data());
	return 1;
}

static int WriteByte(lua_State *L) {
	WriteMemory<byte>(GetPointer(L, 1), (byte)GetInteger(L, 2));
	return 0;
}

static int WriteWord(lua_State *L) {
	WriteMemory<word>(GetPointer(L, 1), (word)GetInteger(L, 2));
	return 0;
}

static int WriteDword(lua_State *L) {
	WriteMemory<dword>(GetPointer(L, 1), (dword)GetInteger(L, 2));
	return 0;
}

static int WriteQword(lua_State *L) {
	WriteMemory<qword>(GetPointer(L, 1), (qword)GetInteger(L, 2));
	return 0;
}

static int WriteFloat(lua_State *L) {
	WriteMemory<float>(GetPointer(L, 1), (float)GetInteger(L, 2));
	return 0;
}

static int WriteDouble(lua_State *L) {
	WriteMemory<double>(GetPointer(L, 1), (double)GetInteger(L, 2));
	return 0;
}

static int WriteBytes(lua_State *L) {
	ptr address = GetPointer(L, 1);
	dword count = lua_gettop(L) - 1;
	for (dword idx = 0; idx < count; idx++) {
		WriteMemory<byte>(address + idx, (byte)GetInteger(L, idx + 2));
	}
	return 0;
}

static int WriteString(lua_State *L) {
	size_t len;
	const char *str = luaL_checklstring(L, 2, &len);
	std::memcpy((void *)GetPointer(L, 1), str, len);
	return 0;
}

static int WriteString1(lua_State *L) {
	String1 *str = new String1(luaL_checkstring(L, 2));
	*(String1 *)GetPointer(L, 1) = *str;
	PushInteger(L, str);		// for cleaning up
	return 1;
}

static int WriteString2(lua_State *L) {
	String2 *str = new String2(luaL_checkstring(L, 2));
	*(String2 *)GetPointer(L, 1) = *str;
	PushInteger(L, str);		// for cleaning up
	return 1;
}

static int FreeString1(lua_State *L) {
	delete (String1 *)GetPointer(L, 1);
	return 0;
}

static int FreeString2(lua_State *L) {
	delete (String2 *)GetPointer(L, 1);
	return 0;
}

static luaL_Reg memorylib[] = {
	{ "ReadByte", ReadByte },
	{ "ReadWord", ReadWord },
	{ "ReadDword", ReadDword },
	{ "ReadQword", ReadQword },
	{ "ReadFloat", ReadFloat },
	{ "ReadDouble", ReadDouble },
	{ "ReadBytes", ReadBytes },
	{ "ReadString", ReadString },
	{ "ReadString1", ReadString1 },
	{ "ReadString2", ReadString2 },
	{ "WriteByte", WriteByte },
	{ "WriteWord", WriteWord },
	{ "WriteDword", WriteDword },
	{ "WriteQword", WriteQword },
	{ "WriteFloat", WriteFloat },
	{ "WriteDouble", WriteDouble },
	{ "WriteBytes", WriteBytes },
	{ "WriteString", WriteString },
	{ "WriteString1", WriteString1 },
	{ "WriteString2", WriteString2 },
	{ "FreeString1", FreeString1 },
	{ "FreeString2", FreeString2 },
	{ NULL, NULL }
};

void luaopen_memory(lua_State *L) {
	luaL_newlib(L, memorylib);
	lua_setglobal(L, "Memory");
}
