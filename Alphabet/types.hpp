#pragma once

#include <string>
#include <cstdint>
#include "lua/lua.hpp"

// Helper Functions //
#define GetPointer(l, i) ((ptr)luaL_checkinteger(l, i))
#define GetInteger(l, i) ((dword)luaL_checkinteger(l, i))
#define GetFloat(l, i) ((double)luaL_checknumber(l, i))
#define PushInteger(l, v) (lua_pushinteger(l, (lua_Integer)v))
#define PushFloat(l, v) (lua_pushnumber(l, (lua_Number)v))

// Types //
using byte = std::uint8_t;
using word = std::uint16_t;
using dword = std::uint32_t;
using qword = std::uint64_t;
using ptr = std::uintptr_t;

namespace Type {
	const dword BYTE = 0;
	const dword WORD = 1;
	const dword DWORD = 2;
	const dword QWORD = 3;
	const dword FLOAT = 4;
	const dword DOUBLE = 5;
	const dword STRING = 6;		// c string
	const dword STRING1 = 7;	// vc++ std::string (with proxy)
	const dword STRING2 = 8;	// vc++ std::string (no proxy)
};

// Globals //
extern std::string path;
extern lua_State *L;	// Lua State
extern void *handle;	// Application Handle
void DebugPrint(const char *msg);
void Assert(bool cond, const char *err);

// Prototypes //
void luaopen_type(lua_State *L);
void luaopen_memory(lua_State *L);
void luaopen_roblox(lua_State *L);
