#include <Windows.h>
#include "lua/lua.hpp"
#include "types.hpp"
#include "string.hpp"

lua_State *L = nullptr;
void *handle = nullptr;
std::string path;

BOOL APIENTRY DllMain(HINSTANCE hDllHandle, DWORD nReason, LPVOID Reserved) {
	if (nReason == DLL_PROCESS_ATTACH) {
		DisableThreadLibraryCalls(hDllHandle);
		handle = hDllHandle;

		path = std::string(getenv("userprofile"));
		std::string file;
		path += "\\Desktop\\Exploit";
		file = path + "\\main.lua";

		L = luaL_newstate();
		luaL_openlibs(L);
		luaopen_type(L);
		luaopen_memory(L);
		luaopen_roblox(L);

		if (luaL_dofile(L, file.c_str()) != LUA_OK) {
			DebugPrint((std::string("[Lua Error]: ") + lua_tostring(L, -1)).c_str());
			lua_pop(L, 1);
			lua_close(L);
			return FALSE;
		}
	}

	return TRUE;
}
