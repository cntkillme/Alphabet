#include <Windows.h>
#include "lua/lua.hpp"
#include "types.hpp"

#define SetType(L, N, V) PushInteger(L, V); lua_setfield(L, -2, N)

void DebugPrint(const char *msg) {
	MessageBox(NULL, msg, "Debug", MB_OK);
}

void Assert(bool cond, const char *err) {
	if (!cond) {
		DebugPrint(err);
		exit(1);
	}
}

void luaopen_type(lua_State *L) {
	// Path Global //
	lua_pushstring(L, path.c_str());
	lua_setglobal(L, "Path");

	// Base Global //
	PushInteger(L, GetModuleHandle(NULL));
	lua_setglobal(L, "Base");

	// Type Global //
	lua_newtable(L);
	SetType(L, "Byte", Type::BYTE);
	SetType(L, "Word", Type::WORD);
	SetType(L, "Dword", Type::DWORD);
	SetType(L, "Qword", Type::QWORD);
	SetType(L, "Float", Type::FLOAT);
	SetType(L, "Double", Type::DOUBLE);
	SetType(L, "String", Type::STRING);
	SetType(L, "String1", Type::STRING1);
	SetType(L, "String2", Type::STRING2);
	lua_setglobal(L, "Type");
}
