#pragma once

#include <string>
#include "types.hpp"

// std::string with proxy (VC++)
class String1 {
private:
	ptr *proxy;
	union {
		char buff[16];
		char *content;
	} data;
	dword size;
	dword capacity;
public:
	String1(const char *string, dword ssize = 0xFFFFFFFF) {
		proxy = new ptr[2];
		proxy[0] = (ptr)this;
		proxy[1] = 0;
		size = (ssize == 0xFFFFFFFF) ? strlen(string) : ssize;
		capacity = size + 1;

		if (size >= 16) {		// not self-contained
			data.content = new char[size + 1];
			std::memcpy(data.content, string, size + 1);
		} else {				// self-contained
			std::memcpy(data.buff, string, size + 1);
		}
	}

	String1(const String1 &string) : String1(string.Data(), string.Size()) { }

	~String1() {
		delete[] proxy;
		if (size >= 16) delete[] data.content;
	}

	const char *Data() const {
		return (size >= 16) ? data.content : data.buff;
	}

	dword Size() const {
		return size;
	}

	dword Capacity() const {
		return capacity;
	}

	const ptr *Proxy() const {
		return proxy;
	}
};

// std::string without proxy (VC++)
class String2 {
private:
	union {
		char buff[16];
		char *content;
	} data;
	dword size;
	dword capacity;
public:
	String2(const char *string, dword ssize = 0xFFFFFFFF) {
		size = (ssize == 0xFFFFFFFF) ? strlen(string) : ssize;
		capacity = size + 1;

		if (size >= 16) {		// not self-contained
			data.content = new char[size + 1];
			std::memcpy(data.content, string, size + 1);
		} else {				// self-contained
			std::memcpy(data.buff, string, size + 1);
		}
	}

	String2(const String2 &string) : String2(string.Data(), string.Size()) { }

	~String2() {
		if (size >= 16) delete[] data.content;
	}

	const char *Data() const {
		return (size >= 16) ? data.content : data.buff;
	}

	dword Size() const {
		return size;
	}

	dword Capacity() const {
		return capacity;
	}
};
